#ifndef CKDTREE_H
#define CKDTREE_H

#include <cmath>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

/************************************************************************************************/
/*************************************** KD-Tree Node *******************************************/
/************************************************************************************************/

// класс kd-дерева
template <class T, size_t D>
class CKDTree;

// класс узла kd-дерева
template <class T, size_t D>
class CKDTreeNode {
  private:

    // указатель на родительский узел
    CKDTreeNode<T, D>* _parent;
    // указатели на дочерние узлы
    CKDTreeNode<T, D>* _lchild; // левый
    CKDTreeNode<T, D>* _rchild; // правый

    size_t _axis;          // размерность разбиения - split dimension
    size_t _dimension = D; // размерность
    size_t _id;            // идентификатор узла
    bool   _flag;          // флаг для обхода
    T      _x[D];          // точка размерности D - N-dimension point

  public:
    T data; // данные узла

    // конструктор
    CKDTreeNode (const T& itemData, size_t& nodeId,
                                    CKDTreeNode<T, D>* parent = NULL,
                                    CKDTreeNode<T, D>* lchild = NULL,
                                    CKDTreeNode<T, D>* rchild = NULL);
    // деструктор
    ~CKDTreeNode(void);

    // методы доступа к полям
    CKDTreeNode<T, D>* Left() const   {return _lchild;}
    CKDTreeNode<T, D>* Parent() const {return _parent;}
    CKDTreeNode<T, D>* Right() const  {return _rchild;}

    // сделать класс BinSTree дружественным для доступа к полям _left и _right
    friend class CKDTree<T, D>;
};
//template <class T> using PTreeNode = CTreeNode<T>*;

/***********************************************************************************************/
// конструктор
template <class T, size_t D>
CKDTreeNode<T, D>::CKDTreeNode(const T& itemData, size_t& nodeId, CKDTreeNode<T, D>* parent, CKDTreeNode<T, D>* lchild, CKDTreeNode<T, D> *rchild)
                          : _parent(parent), _lchild(lchild), _rchild(rchild), _id(nodeId), _flag(false), data(itemData)
{

}

// деструктор
template<class T, size_t D>
CKDTreeNode<T, D>::~CKDTreeNode(void)
{
  if (_lchild) delete _lchild;
  if (_rchild) delete _rchild;
}

/************************************************************************************************/
/**************************************** KD-TREE ***********************************************/
/************************************************************************************************/
template <class T, size_t D>
class CKDTree
{

private:
    CKDTreeNode<T,D>* _root;  // корень дерева
    size_t            _count; // количеств узлов в дереве
    //int (*_cmpNodes)(T*,T*);  // функция сравнения

protected:
    // функции обработки узлов дерева
    //int basicCmpNodes(T* point1, T* point2); // сравнение узлов дерева

    void Delete(CKDTreeNode<T,D>* node); // удалить узел дерева

public:
    CKDTree();
    //CKDTree(int (*cmpNodes)(T*, T*));
    ~CKDTree();

    // действия с узлами дерева
    size_t       Count() const {return _count;}            // количество вершин в дереве
    void         Clear();                                  // очистить дерево
    void         Delete(T data);                           // удалить узел дерева
    double       distance(CKDTreeNode<T, D>& point1, CKDTreeNode<T, D>& point2, int type); // поиск расстояни между двумя точками
    CKDTreeNode<T,D>* FindNearest(T* point, int distType); // поиск ближайшего элемента в дереве
    CKDTreeNode<T,D>* FindNearest(CKDTreeNode<T,D>* node, int distType); // поиск ближайшего элемента в дереве
    void         InsertNode(int data);                     // добавление вершины в дерево
    void         InsertNode(CKDTreeNode<T,D>* node);
    bool         IsEmpty() const {return _root == NULL;} // дерево не пустое

    // действия с функциями дерева
    //void         setCmpNodesFunction(int (*cmpNodes)(T, T)){_cmpNodes = cmpNodes;}
    //void       setProcessNodeFunction(void (*processNode)(T, T)){_processNode = processNode;}
};

/***********************************************************************************************/

/**********************************************/
/********** создание/удаление дерева **********/
/**********************************************/

// конструктор
template <class T, size_t D>
CKDTree<T, D>::CKDTree() : _root(NULL), _count(0)
{

}

/**********************************************/
/******* функции обработки узлов дерева *******/
/**********************************************/

// поиск расстояни между двумя точками - find distance between two points
template <typename T, size_t D>
double CKDTree<T, D>::distance(CKDTreeNode<T, D>& point1, CKDTreeNode<T, D>& point2, int type)
{
    size_t dim          = D;
    double term, result = 0;

    switch (type)
    {
        case 0: // Eвклидово расстояние - Euclidean metric
                while (dim--) {
                    term = point1.x[dim] - point2.x[dim];
                    result += term * term;
                }
                return sqrt(result);

        case 1: // Манхеттонское расстояние - Manhattan metric
                while (dim--) {
                    term = point1.x[dim] - point2.x[dim];
                    result += abs(term);
                }
    }
}



#endif // CKDTREE_H
