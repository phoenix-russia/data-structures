﻿#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>
#include <queue>
#include <stack>
#include <stdio.h>
#include <stdlib.h>

/************************************************************************************************/
/********************************* Binary Search Tree Node **************************************/
/************************************************************************************************/

// типы дочерних узлов
enum TbtChildType {ctLeft, // левый дочерний узел
                   ctRight // правый дочерний узел
                  };

// цвета для красно-черного дерева
enum TbtRBColor {rbBlack, // черный
                 rbRed    // красный
                };

// класс бинарного дерева поиска
template <class T>
class BinarySearchTree;

// класс узла бинарного дерева поиска
template <class T>
class CBinTreeNode {
  private:
    // указатель на родительский узел
    CBinTreeNode<T>* _parent;
    // указатели на дочерние узлы
    CBinTreeNode<T>* _lchild; // левый
    CBinTreeNode<T>* _rchild; // правый

    bool _flag; // флаг для обхода
  public:
    T data; // данные узла

    // конструктор
    CBinTreeNode (const T& itemData, CBinTreeNode<T>* parent = NULL,
                                     CBinTreeNode<T>* lchild = NULL,
                                     CBinTreeNode<T>* rchild = NULL);
    // деструктор
    ~CBinTreeNode(void);

    // методы доступа к полям
    CBinTreeNode<T>* Left() const   {return _lchild;}
    CBinTreeNode<T>* Parent() const {return _parent;}
    CBinTreeNode<T>* Right() const  {return _rchild;}

    // сделать класс BinSTree дружественным для доступа к полям _left и _right
    friend class BinarySearchTree<T>;
};
//template <class T> using PTreeNode = CTreeNode<T>*;

/***********************************************************************************************/
// конструктор
template <class T>
CBinTreeNode<T>::CBinTreeNode(const T& itemData, CBinTreeNode<T>* parent, CBinTreeNode<T>* lchild, CBinTreeNode<T> *rchild)
                          : _parent(parent), _lchild(lchild), _rchild(rchild), _flag(false), data(itemData)
{

}

// деструктор
template<class T>
CBinTreeNode<T>::~CBinTreeNode(void)
{
  if (_lchild) delete _lchild;
  if (_rchild) delete _rchild;
}

/************************************************************************************************/
/*********************************** Binary Search Tree *****************************************/
/************************************************************************************************/

template <class T>
class BinarySearchTree {
    private:
        CBinTreeNode<T>* _root;  // корень дерева
        size_t           _count; // количеств узлов в дереве
        int (*_cmpNodes)(T,T);   // функция сравнения
        //void (*_processNode) (CTreeNode<T>* CTreeNode); // функция обрабтки узлов

    protected:
        // функции обработки узлов дерева
        int basicCmpNodes(T &value1, T &value2); // сравнение узлов дерева

        void Delete(CBinTreeNode<T>* node); // удалить узел дерева

    public:
        BinarySearchTree();
        BinarySearchTree(int (*cmpNodes)(T, T));
        ~BinarySearchTree();

        // действия с узлами дерева
        size_t       Count() const {return _count;}          // количество вершин в дереве
        void         Clear();                                // очистить дерево
        void         Delete(T data);                         // удалить узел дерева
        CBinTreeNode<T>* Find(T data);                       // поиск элемента в дереве
        CBinTreeNode<T>* FindMin();                          // поиск наименьшего элемента в дереве
        void         InsertNode(int data);                   // добавление вершины в дерево
        void         InsertNode(CBinTreeNode<T>* node);
        bool         IsEmpty() const {return _root == NULL;} // дерево не пустое

        // действия с функциями дерева
        void         setCmpNodesFunction(int (*cmpNodes)(T, T)){_cmpNodes = cmpNodes;}
        //void       setProcessNodeFunction(void (*processNode)(T, T)){_processNode = processNode;}


        // обход дерева
        void TraverseInOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*));   // симметричный обход (рекурсивный)
        void TraversePreOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*));  // обход в ширину     (рекурсивный)
        void TraversePostOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*)); // обход в глубину    (рекурсивный)

        void TraverseInOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*));    // симметричный обход (нерекурсивный)
        void TraverseLevelOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*)); // обход по уровням   (нерекурсивный)
        void TraversePreOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*));   // обход в ширину     (нерекурсивный)
        void TraversePostOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*));  // обход в глубину    (нерекурсивный)
};

/***********************************************************************************************/

/**********************************************/
/********** создание/удаление дерева **********/
/**********************************************/

// конструктор
template <class T>
BinarySearchTree<T>::BinarySearchTree() : _root(NULL), _count(0), _cmpNodes(&basicCmpNodes)
{

}

template <class T>
BinarySearchTree<T>::BinarySearchTree(int (*cmpNodes)(T, T)) : _root(NULL), _count(0), _cmpNodes(cmpNodes)
{

}

// деструктор
template <class T>
BinarySearchTree<T>::~BinarySearchTree()
{
    if (_root) delete _root;
}

/**********************************************/
/******* функции обработки узлов дерева *******/
/**********************************************/

// сравнение узлов дерева
template <class T>
int BinarySearchTree<T>::basicCmpNodes(T &value1, T &value2)
{
    if (value1 > value2)
        return 1;
    else
        if (value1 < value2)
            return -1;
        else
            return 0;
}

/**********************************************/
/********** действия с узлами дерева **********/
/**********************************************/

// удалить узел дерева
template <class T>
void BinarySearchTree<T>::Delete(T data)
{
    // ...
}

// удалить узел дерева
template <class T>
void BinarySearchTree<T>::Delete(CBinTreeNode<T>* node)
{
    // ...
}

// поиск элемента в дереве
template <class T>
CBinTreeNode<T>* BinarySearchTree<T>::Find(T data)
{
    CBinTreeNode<T>* currentNode = _root;
    while (currentNode)
    {
        int result = _cmpNodes(data, currentNode->data);
        if (result < 0)
            currentNode = currentNode->Left();
        else
            if (result > 0)
                currentNode = currentNode->Right();
            else
                return currentNode->data;
    }
    return NULL;
}

// поиск наименьшего элемента в дереве
template <class T>
CBinTreeNode<T>* BinarySearchTree<T>::FindMin()
{
    CBinTreeNode<T>* currentNode = _root;

    if (currentNode == NULL)
        return NULL;

    while (currentNode->Left())
        currentNode = currentNode->Left();
    return currentNode;
}

// добавление узла в дерево
template <class T>
void BinarySearchTree<T>::InsertNode(int data)
{
    CBinTreeNode<T>* newNode = new CBinTreeNode<T>(data);
    InsertNode(newNode);
}

// добавление узла в дерево
template <class T>
void BinarySearchTree<T>::InsertNode(CBinTreeNode<T>* node)
{
    if (_root == NULL)
    {
        _root = node;
        _root->_parent = NULL;
        _root->_lchild = NULL;
        _root->_rchild = NULL;
    }
    else
    {
        // ...qw....
    }

    // увеличить количество узлов дерева на 1
    this->_count += 1;
    return;

}

/************************************************/
/********** рекурсивные обходы деревье **********/
/************************************************/
// симметричный обход
template <class T>
void BinarySearchTree<T>::TraverseInOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    if (!TreePtr)
        return;

    TraverseInOrder(TreePtr->Left(), processNode);
    processNode(this->_root);
    TraverseInOrder(TreePtr->Right(), processNode);

}

// обход в ширину
template <class T>
void BinarySearchTree<T>::TraversePreOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    if (!TreePtr)
        return;

    processNode(_root);
    TraversePreOrder(TreePtr->Left(), processNode);
    TraversePreOrder(TreePtr->Right(), processNode);

}

// обход в глубину
template <class T>
void BinarySearchTree<T>::TraversePostOrder(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    if (!TreePtr)
        return;

    TraversePostOrder(TreePtr->Left(), processNode);
    TraversePostOrder(TreePtr->Right(), processNode);
    processNode(_root);

}

/**************************************************/
/********** нерекурсивные обходы деревье **********/
/**************************************************/
// симметричный обход
template <class T>
void BinarySearchTree<T>::TraverseInOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    // ...
}

// обход по уровням
template <class T>
void BinarySearchTree<T>::TraverseLevelOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    // ...
}

// обход в ширину
template <class T>
void BinarySearchTree<T>::TraversePreOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    if (!TreePtr) return;

    // предположим, что мы не добрались до выбранного узла

    // создать стек
    std::stack<CBinTreeNode<T>*> stack;
    try
    {

    }
    catch (...)
    {
        // уничтожить стек
        delete stack;
    }
}

// обход в глубину
template <class T>
void BinarySearchTree<T>::TraversePostOrderNoRec(CBinTreeNode<T>* TreePtr, void (*processNode) (CBinTreeNode<T>*))
{
    // ...
}

#endif
